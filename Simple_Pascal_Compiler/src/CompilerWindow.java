import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

@SuppressWarnings("serial")
public class CompilerWindow extends JFrame{
	
//	private JFrame frame;
    private JButton rewrite_Button;
    private JButton compile_Button;
    private JTextPane jTextPane;
    private JButton close_Button;
    private JPanel jPanel;
    //private JPanel button_Panel;
    private JScrollPane jScrollPane;
    private GroupLayout layout;

    public CompilerWindow() {

        jPanel = new JPanel();
      //  button_Panel = new  JPanel();
        jScrollPane = new JScrollPane();
        jTextPane = new JTextPane();
        compile_Button = new JButton();
        rewrite_Button = new JButton();
        close_Button = new JButton();
              
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Shaon's Simple Pascal Compiler-SSPComp");
       
        jPanel.setBackground(new java.awt.Color(238, 241, 238));
        jPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder
        		(new java.awt.Color(0, 0, 0)), "Write code in this editor"));

        jTextPane.setEditorKit(new javax.swing.text.StyledEditorKit());

        jTextPane.requestFocus();
        jTextPane.setFont(new java.awt.Font("Sans-Serif", 0, 14)); 
        jScrollPane.setViewportView(jTextPane);

        layout = new GroupLayout(jPanel);
        jPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane, GroupLayout.DEFAULT_SIZE, 518, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane, GroupLayout.DEFAULT_SIZE, 308, Short.MAX_VALUE)
                .addContainerGap())
        );


        compile_Button.setText("Compile");
        compile_Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                compile_ButtonActionPerformed(evt);
            }
        });

        rewrite_Button.setText("Rewrite");
        rewrite_Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                rewrite_ButtonActionPerformed(evt);
            }
        });

        close_Button.setText("Close");
        close_Button.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                close_ButtonActionPerformed(evt);
            }
        });
 
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))     
        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                    .addComponent(rewrite_Button, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                   .addComponent(compile_Button,GroupLayout.DEFAULT_SIZE, 78, Short.MAX_VALUE)
                    .addComponent(close_Button, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(19, Short.MAX_VALUE)
        ));
        
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(compile_Button)
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addComponent(rewrite_Button)
                        .addPreferredGap(ComponentPlacement.UNRELATED)
                        .addComponent(close_Button))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(ComponentPlacement.UNRELATED)
                .addContainerGap())
        );

       pack();
    }

    private void compile_ButtonActionPerformed(ActionEvent evt) {
    	  CheckingCode chk=new CheckingCode();
           String txt=jTextPane.getText();
       String str = chk.tokenzString(txt);

      JOptionPane.showMessageDialog(jPanel, str,"Program output",
    		  JOptionPane.PLAIN_MESSAGE);
    }
  
    private void rewrite_ButtonActionPerformed(ActionEvent evt) {
        jTextPane.setText("");
     }
 
    private void close_ButtonActionPerformed(ActionEvent evt) {
           System.exit(0);
    }
   
    public static void main(String args[]) {
  	
        try {
            for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CompilerWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(CompilerWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(CompilerWindow.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(CompilerWindow.class.getName()).log(Level.SEVERE, null, ex);
        }


        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new CompilerWindow().setVisible(true);
            }
        });
    }


}
